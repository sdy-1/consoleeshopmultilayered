﻿using System.Collections.Generic;
using System.Linq;

namespace OnlineShop.Data.Models
{
    public enum OrderStatus
    {
        New,
        Paid,
        Sent,
        Received,
        Completed,
        Canceled
    }
    
    public class Order
    {
        public List<ProductOrder> Products { get; } = new List<ProductOrder>();

        public OrderStatus Status { get; set; } = OrderStatus.New;

        public decimal TotalPrice => Products.Sum(p => p.TotalPrice);

        public override string ToString()
        {
            var toString = Products.Aggregate(string.Empty, (current, product) => current + product);

            toString += $"\nTotal: {TotalPrice}";
            toString += $"\nStatus: {Status}";

            return toString;
        }
    }
}