﻿using System.Collections.Generic;
using OnlineShop.Data.Models;

namespace OnlineShop.Data.Repositories
{
    public class CustomerRepository
    {
        private List<Customer> _customers = new List<Customer>();
        
        public IEnumerable<Customer> GetAllCustomers() => _customers;

        public CustomerRepository()
        {
            var array = new []
            {
                new Customer {Login = "customer1", Password = "pass1"},
                new Customer {Login = "customer2", Password = "pass2"},
                new Customer {Login = "customer3", Password = "pass3"}
            };
            
            _customers.AddRange(array);
        }
        
        public void Create(Customer customer) =>
            _customers.Add(customer);
    }
}