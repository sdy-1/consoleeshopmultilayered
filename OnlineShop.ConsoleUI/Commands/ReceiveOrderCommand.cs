﻿using System;
using System.Linq;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands
{
    public class ReceiveOrderCommand : CommandBase
    {
        public override string Name => "receive order";
        public override string Description => "Change status of order to \"Received\"";
        
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var customerController = (CustomerController) controllerBase;
            var service = customerController.CustomerService;
            
            var orders = service.GetPlacedOrders();

            int number = 1;
            foreach (var order in orders)
            {
                Console.Write($"{number++}. ");
                foreach (var product in order.Products)
                {
                    Console.WriteLine($"{product}");
                }

                Console.WriteLine($"Total price: {order.TotalPrice}");
                Console.WriteLine($"Status: {order.Status}\n");
            }

            Console.WriteLine("Choose an order you have received");
            int choose;
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out choose)
                    && choose >= 1 && choose <= number - 1)
                    break;
                
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Choose the correct order number");
                Console.ResetColor();
            }

            service.ChangeStatus(choose - 1);
            Console.WriteLine(orders.ElementAt(choose - 1));

            return customerController;
        }
    }
}