﻿using System;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands
{
    public class GetAllProductsCommand : CommandBase
    {
        public override string Name => "get products";
        public override string Description => "Shows all products in the shop";
        
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var service = controllerBase.Service;

            var products = service.GetAllProduct();
            foreach (var product in products)
            {
                Console.WriteLine(product);
            }

            return controllerBase;
        }
    }
}