﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;
using OnlineShop.Data.Models;

namespace OnlineShop.ConsoleUI.Commands
{
    public class CreateProductCommand : CommandBase
    {
        public override string Name => "create product";
        public override string Description => "Adds a new product in the shop";
        
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var adminController = (AdminController) controllerBase;
            var service = adminController.AdminService;

            var products = service.GetAllProduct();
            
            var name = SetName(products);

            var category = SetCategory();

            var description = SetDescription();

            var price = SetPrice();

            service.AddProduct(name, price, category, description);
            Console.WriteLine("You have successfully added a new product to the shop");

            return adminController;
        }

        private static decimal SetPrice()
        {
            Console.WriteLine("Enter a price of product");
            decimal price;
            while (true)
            {
                if (decimal.TryParse(Console.ReadLine(), out price) && price > 0)
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Wrong price input");
                Console.ResetColor();
            }

            return price;
        }

        private static string SetDescription()
        {
            Console.WriteLine("Write a description");
            var description = Console.ReadLine();
            return description;
        }

        private static Category SetCategory()
        {
            Console.WriteLine("Choose category of a product");
            foreach (var categoryName in Enum.GetNames(typeof(Category)))
            {
                Console.WriteLine(categoryName);
            }

            Category category;
            while (true)
            {
                if (Enum.TryParse(Console.ReadLine(), out category))
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("This category does not exist");
                Console.ResetColor();
            }

            return category;
        }

        private static string SetName(IEnumerable<Product> products)
        {
            string name;
            while (true)
            {
                Console.WriteLine("Enter a name of product");
                name = Console.ReadLine();

                if (name is { } && !products.Any(p => p.Name == name))
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The product with this name is already exists");
                Console.ResetColor();
            }

            return name;
        }
    }
}