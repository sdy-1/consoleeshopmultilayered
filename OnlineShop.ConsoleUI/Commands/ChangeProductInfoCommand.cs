﻿using System;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;
using OnlineShop.Data.Models;

namespace OnlineShop.ConsoleUI.Commands
{
    public class ChangeProductInfoCommand : CommandBase
    {
        public override string Name => "change product";
        public override string Description => "Changes information about product";

        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var adminController = (AdminController) controllerBase;
            var service = adminController.AdminService;

            Console.WriteLine("Enter a product name or \"0\" to leave");

            string name;
            while (true)
            {
                name = Console.ReadLine();
                Console.WriteLine();
                switch (name)
                {
                    case "0":
                        break;

                    default:
                        if (service.FindProduct(name) is { })
                        {
                            service.ChangeProductInfo(SetDescription(), SetPrice(), name);
                            Console.WriteLine("You successfully change product info");
                            break;
                        }

                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("There is no customer with such login");
                        Console.ResetColor();
                        continue;
                }
                break;
            }

            return adminController;
        }
        
        private static decimal SetPrice()
        {
            Console.WriteLine("Enter a price of product");
            decimal price;
            while (true)
            {
                if (decimal.TryParse(Console.ReadLine(), out price) && price > 0)
                    break;

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Wrong price input");
                Console.ResetColor();
            }

            return price;
        }

        private static string SetDescription()
        {
            Console.WriteLine("Write a description");
            var description = Console.ReadLine();
            return description;
        }
    }
}