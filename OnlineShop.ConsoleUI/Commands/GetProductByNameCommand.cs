﻿using System;
using System.Transactions;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers;
using OnlineShop.ConsoleUI.Controllers.Abstract;

namespace OnlineShop.ConsoleUI.Commands
{
    public class GetProductByNameCommand : CommandBase
    {
        public override string Name => "get product";
        public override string Description => "Shows product info by its name";
        
        public override ControllerBase Execute(ControllerBase controllerBase)
        {
            var service = controllerBase.Service;

            Console.WriteLine("Enter a product name or \"0\" to exit");
            while (true)
            {
                var name = Console.ReadLine();
                switch (name)
                {
                    case "0":
                        return controllerBase;

                    default:
                        Console.WriteLine(service.FindProduct(name)?.ToString()
                        ?? "No product with such name");
                        
                        break;
                }

                break;
            }

            return controllerBase;
        }
    }
}