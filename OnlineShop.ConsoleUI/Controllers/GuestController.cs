﻿using System.Collections.Generic;
using OnlineShop.ConsoleUI.Commands;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers.Abstract;
using OnlineShop.Data.Repositories;
using OnlineShop.Domain.Services;

namespace OnlineShop.ConsoleUI.Controllers
{
    public class GuestController : ControllerBase
    {
        public GuestService GuestService { get; }

        public override List<CommandBase> Commands { get; set; } = new List<CommandBase>
        {
            new SignUpCommand(),
            new HelpCommand(),
            new GetAllProductsCommand(),
            new GetProductByNameCommand(),
            new SignInCommand()
        };

        public GuestController(AdminRepository adminRepository, CustomerRepository customerRepository,
            ProductRepository productRepository)
            : base(adminRepository, customerRepository, productRepository)
        {
            GuestService = new GuestService(adminRepository, customerRepository, productRepository);
        }
    }
}