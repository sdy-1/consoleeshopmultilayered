﻿using System.Collections.Generic;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.Data.Repositories;
using OnlineShop.Domain.Services;

namespace OnlineShop.ConsoleUI.Controllers.Abstract
{
    public abstract class ControllerBase
    {
        public ServiceBase Service { get; } 
    
        public AdminRepository AdminRepository { get; set; }
        public CustomerRepository CustomerRepository { get; set; }
        public ProductRepository ProductRepository { get; set; }
        
        public abstract List<CommandBase> Commands { get; set; }
        
        protected ControllerBase(AdminRepository adminRepository, CustomerRepository customerRepository,
            ProductRepository productRepository)
        {
            AdminRepository = adminRepository;
            CustomerRepository = customerRepository;
            ProductRepository = productRepository;

            Service = new ServiceBase(customerRepository, productRepository);
        }
    }
}