﻿using System.Collections.Generic;
using OnlineShop.ConsoleUI.Commands;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers.Abstract;
using OnlineShop.Data.Repositories;
using OnlineShop.Domain.Services;

namespace OnlineShop.ConsoleUI.Controllers
{
    public class CustomerController : ControllerBase
    {
        public CustomerService CustomerService { get; }

        public override List<CommandBase> Commands { get; set; } = new List<CommandBase>
        {
            new HelpCommand(),
            new GetAllProductsCommand(),
            new GetProductByNameCommand(),
            new AddToOrderCommand(),
            new CheckoutCommand(),
            new CancelCurrentOrderCommand(),
            new OrderHistoryCommand(),
            new ChangeMyInfoCommand(),
            new LogOutCommand(),
            new ReceiveOrderCommand()
        };

        public CustomerController(AdminRepository adminRepository, CustomerRepository customerRepository,
            ProductRepository productRepository, string customerLogin)
            : base(adminRepository, customerRepository, productRepository)
        {
            CustomerService = new CustomerService(customerLogin, customerRepository, productRepository);
        }
    }
}