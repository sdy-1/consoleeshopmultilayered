﻿using System.Collections.Generic;
using OnlineShop.ConsoleUI.Commands;
using OnlineShop.ConsoleUI.Commands.Abstract;
using OnlineShop.ConsoleUI.Controllers.Abstract;
using OnlineShop.Data.Repositories;
using OnlineShop.Domain.Services;

namespace OnlineShop.ConsoleUI.Controllers
{
    public class AdminController : ControllerBase
    {
        public AdminService AdminService { get; set; }

        public override List<CommandBase> Commands { get; set; } = new List<CommandBase>
        {
            new GetAllProductsCommand(),
            new GetProductByNameCommand(),
            new ChangeCustomerInfoCommand(),
            new HelpCommand(),
            new GetAllCustomersCommand(),
            new CreateProductCommand(),
            new ChangeProductInfoCommand(),
            new ChangeOrderStatusCommand(),
            new LogOutCommand()
        };

        public AdminController(AdminRepository adminRepository, CustomerRepository customerRepository,
            ProductRepository productRepository)
            : base(adminRepository, customerRepository, productRepository)
        {
            AdminService = new AdminService(customerRepository, productRepository);
        }
    }
}