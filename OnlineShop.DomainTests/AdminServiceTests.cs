﻿using System.Linq;
using NUnit.Framework;
using OnlineShop.Data.Models;
using OnlineShop.Data.Repositories;
using OnlineShop.Domain.Services;

namespace OnlineShop.DomainTests
{
    public class AdminServiceTests
    {
        private AdminRepository _adminRepository = new AdminRepository();
        private CustomerRepository _customerRepository = new CustomerRepository();
        private ProductRepository _productRepository = new ProductRepository();
        private AdminService _admservice;
        
        [SetUp]
        public void Setup()
        {
            _admservice = new AdminService(_customerRepository, _productRepository);
        }
        
        [Test]
        public void CreateAdminRepo_ReturnsRepoWithAdmin()
        {
            decimal price = 10;
            Category category = Category.Drink;
            const string name = "Beer";
            string description = "desc";
            _admservice.AddProduct(name, price, category, description);
            Assert.AreEqual(_productRepository.GetAllProducts().ToArray()[5].Name, "Beer");
        }

        [Test]
        public void GetAllCustomers_ReturnsCorrectValue()
        {
            var check = _admservice.GetAllCustomers();
            Assert.AreEqual(check.Count(), _customerRepository.GetAllCustomers().Count());
        }

        [Test]
        public void FindCustomer_ReturnsCorrectCustomer()
        {
            var cus = _admservice.FindCustomer("customer1");
            Assert.AreEqual(cus.Login, _customerRepository.GetAllCustomers().ToArray()[0].Login);
        }

        [Test]
        public void ChangeInfoAboutCustomer_ReturnsCustomerWithChangedInfo()
        {
            string name = "vasya";
            string email = "sda@gmail.com";
            string number = "0000000";
            string adress = "sda";
            string customerLog = "customer1";
            _admservice.ChangeInfo(name, email, number, adress, customerLog);
            Assert.AreEqual("vasya", _customerRepository.GetAllCustomers().ToArray()[0].Name);
        }

        [Test]
        public void ChangeInfoAboutProduct_ChangesInfo()
        {
            string name = "Apple";
            string desc = "nice apple";
            decimal price = 21212;
            _admservice.ChangeProductInfo(desc, price, name);
            Assert.AreEqual("nice apple", _productRepository.GetAllProducts().ToArray()[0].Description);
        }

        [Test]
        public void ChangeStatus_ChangesStatusCorrectly()
        {
            _customerRepository.GetAllCustomers().ToList()[0].PlacedOrders.Add(new Order(){Status = OrderStatus.Paid});
            _admservice.ChangeStatus(0, OrderStatus.Completed, "customer1");
            Assert.AreEqual(OrderStatus.Completed, _customerRepository.GetAllCustomers().ToArray()[0].PlacedOrders[0].Status);

        }
    }
}