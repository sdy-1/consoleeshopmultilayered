﻿using System.Collections.Generic;
using System.Linq;
using OnlineShop.Data.Models;
using OnlineShop.Data.Repositories;
using OnlineShop.Domain.Services.Interfaces;

namespace OnlineShop.Domain.Services
{
    public class ServiceBase : IProductSearchable
    {
        protected ProductRepository ProductRepository { get; }
        protected CustomerRepository CustomerRepository { get; }
        
        public ServiceBase(CustomerRepository customerRepository, ProductRepository productRepository) =>
            (CustomerRepository, ProductRepository) = (customerRepository, productRepository);
        
        public IEnumerable<Product> GetAllProduct() =>
            ProductRepository.GetAllProducts();

        public Product FindProduct(string name) =>
            GetAllProduct().FirstOrDefault(p => p.Name == name);
    }
}