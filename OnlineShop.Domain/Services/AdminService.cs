﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;
using OnlineShop.Data.Models;
using OnlineShop.Data.Repositories;
using OnlineShop.Domain.Services.Interfaces;

namespace OnlineShop.Domain.Services
{
    public class AdminService : ServiceBase, IChangeInfo, IChangeStatusOrder
    {
        public AdminService(CustomerRepository customerRepository, ProductRepository productRepository) 
            : base(customerRepository, productRepository) { }

        public void AddProduct(string name, decimal price, Category category, string description)
        {
            var product = new Product(name, price, description, category);
            ProductRepository.Create(product);
        }
        
        public IEnumerable<Customer> GetAllCustomers() =>
            CustomerRepository.GetAllCustomers();
        
        public Customer FindCustomer(string login) =>
            GetAllCustomers().FirstOrDefault(p => p.Login == login);

        public void ChangeInfo(string name, string email, string number, string address, string customerLogin)
        {
            var customer = FindCustomer(customerLogin);

            customer.Name = name;
            customer.Email = email;
            customer.PhoneNumber = number;
            customer.Address = address;
        }

        public void ChangeProductInfo(string description, decimal price, string name)
        {
            var product = ProductRepository.GetAllProducts()
                .First(p => p.Name == name);

            product.Description = description;
            product.Price = price;
        }

        public void ChangeStatus(int orderNumber, OrderStatus orderStatus, string customerLogin)
        {
            var customer = FindCustomer(customerLogin);
            customer.PlacedOrders[orderNumber].Status = orderStatus;
        }
    }
}